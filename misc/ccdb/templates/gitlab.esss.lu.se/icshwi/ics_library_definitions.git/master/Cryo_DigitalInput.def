###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_DO                                                               ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                             DIO - Single Digital Input/Output                            ##
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0             ########################################
# Author:  Saeed, Miklos Boros 
# Date:    20-11-2019
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

add_digital("OpMode_Auto",              ARCHIVE=True, PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",            ARCHIVE=True, PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("Opened",                   ARCHIVE=True, PV_DESC="Opened",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("Closed",                   ARCHIVE=True, PV_DESC="Closed",                    PV_ONAM="True",           PV_ZNAM="False")

#Alarm signals
add_major_alarm("Alarm","Alarm",           PV_ZNAM="NominalState")
add_minor_alarm("Warning","Warning",       PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")

add_digital("Cmd_ManuOpen",            PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",           PV_DESC="CMD: Manual Close")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Signal Name and Description
add_string("SignalName", 30, PV_VAL="[PLCF#SignalName0]", PV_PINI="YES")
add_string("SignalDesc", 30, PV_VAL="[PLCF#SignalDescription0]", PV_PINI="YES")

#VacOK Setpoint
add_analog("VGP_1000_HI",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_1000_LO",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_2000_HI",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_2000_LO",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_3000_HI",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_3000_LO",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_4000_HI",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")
add_analog("VGP_4000_LO",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="Compare level of cavity vacuum")

#IsoVac OK
add_analog("IsoVacOK_lvl_VGP_HI",   "REAL", PV_PREC="2",        ARCHIVE=True,              PV_DESC="Compare level of Isovacuum high")
add_analog("IsoVacOK_lvl_VGP_LO",   "REAL", PV_PREC="2",        ARCHIVE=True,              PV_DESC="Compare level of Isovacuum low")
add_analog("IsoVacOK_lvl_VGC_HI",   "REAL", PV_PREC="2",        ARCHIVE=True,              PV_DESC="Compare level of Isovacuum high")
add_analog("IsoVacOK_lvl_VGC_LO",   "REAL", PV_PREC="2",        ARCHIVE=True,              PV_DESC="Compare level of Isovacuum low")


#External signals
add_digital("CDS_Cryo_OK",             ARCHIVE=True,       PV_DESC="Cryo system OK 2K",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ext_Reserve_01",          ARCHIVE=True,       PV_DESC="Cryo system OK 2K",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ext_Reserve_02",          ARCHIVE=True,       PV_DESC="Cryo system OK 2K",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ext_Reserve_03",          ARCHIVE=True,       PV_DESC="Cryo system OK 2K",         PV_ONAM="True",           PV_ZNAM="False")

